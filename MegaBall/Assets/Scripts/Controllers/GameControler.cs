﻿using System.Collections.Generic;
using UnityEngine;

public class GameControler : MonoBehaviour
{
    [Header("Views")]
    [SerializeField] private ViewBoard viewBoard;
    [SerializeField] private ViewBall viewBall;
    [SerializeField] private ViewUI viewUI;

    private PlayerInput playerInput;

    private IPlayerInput input;
    private IViewBoard board;
    private IViewBall ball;
    private IViewUI ui;

    private GameState state;

    #region MonoBehaviour methods
    private void Awake()
    {
        board = viewBoard.GetComponent<IViewBoard>();
        ball = viewBall.GetComponent<IViewBall>();
        ui = viewUI.GetComponent<IViewUI>();
        input = new PlayerInput();
    }

    private void Start()
    {
        SetState(GameState.Start);

        ui.SetVisibilityPlayWindow(true);
        ui.SetPlayAction(SetUpBoard);
        ui.SetContinueAction(SetUpBoard);

        ball.SetReachDestinationAction(ReachedDestination);
        ball.SetPartOfWayReachedAction(ReachedPosition);
    }

    private void Update()
    {
        switch (state)
        {
            case GameState.CheckInput:
                {
                    var direction = input.Direction;
                    if (board.CanMoveToDirection(direction, ball.Positions))
                    {
                        var pathList = new List<Vector3>();
                        var path = board.GetPath(direction, 1, pathList, ball.Positions);
                        if (path != null)
                        {
                            ball.MoveTo(path.ToArray());
                            SetState(GameState.BallMoving);
                        }
                    }
                    break;
                }
        }
    }
    #endregion

    #region Ball methods
    private void ReachedDestination()
    {
        if (board.IsAllAreaOpened)
        {
            board.RotateCubes(180);
            ui.SetVisibilityContinueWindow(true);
            SetState(GameState.Stop);
        }
        else
        {
            SetState(GameState.CheckInput);
        }
    }

    private void ReachedPosition(Vector3 position)
    {
        board.OpenArea(position);
    }
    #endregion

    #region Board methods
    private void SetUpBoard()
    {
        SetState(GameState.Start);
        board.SetUpBoard(() => SetState(GameState.CheckInput));
        ball.SetStartPosition(board.GetStartBallPosition());
        board.RotateCubes(0);
    }
    #endregion

    #region Controller
    private void SetState(GameState state)
    {
        this.state = state;
    }
    #endregion
}

public enum GameState
{
    Start,
    CheckInput,
    BallMoving,
    Stop
}