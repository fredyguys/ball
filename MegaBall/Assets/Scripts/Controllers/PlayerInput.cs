﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInput : IPlayerInput
{
    private Vector3 startTouchPosition;

    private float minSwipeLength = 50;

    public MovementDirection Direction => CheckInput();

    private MovementDirection CheckInput()
    {
        if (Input.GetMouseButtonDown(0))
        {
            startTouchPosition = Input.mousePosition;
        }
        if (Input.GetMouseButton(0))
        {
            var touchPosition = Input.mousePosition;
            var swipeDistance = Vector3.Distance(startTouchPosition, touchPosition);

            if (swipeDistance > minSwipeLength)
            {
                var direction = startTouchPosition - touchPosition;
                direction = direction.normalized;

                if (direction.x > 0.5f && (direction.y < 0.5f && direction.y > -0.5f))
                    return MovementDirection.Left;
                if (direction.x < -0.5f && (direction.y < 0.5f && direction.y > -0.5f))
                    return MovementDirection.Right;
                if ((direction.x < 0.5f && direction.x > -0.5f) && direction.y > 0.5)
                    return MovementDirection.Down;
                if ((direction.x < 0.5f && direction.x > -0.5f) && direction.y < -0.5f)
                    return MovementDirection.Up;
            }
        }
        return MovementDirection.None;
    }
}

public enum MovementDirection
{
    Up,
    Down,
    Left,
    Right,
    None
}