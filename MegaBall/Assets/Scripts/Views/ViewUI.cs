﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class ViewUI : MonoBehaviour, IViewUI
{
    [Header("Buttons")]
    [SerializeField] private Button playBtn;
    [SerializeField] private Button continueBtn;
    [Header("GameObjects")]
    [SerializeField] private GameObject background;
    [SerializeField] private GameObject playObj;
    [SerializeField] private GameObject continueObj;


    public void SetPlayAction(Action onPlay)
    {
        playBtn.onClick.AddListener(() =>
        {
            onPlay();
            SetVisibilityPlayWindow(false);
        });
    }
    public void SetContinueAction(Action onContinue)
    {
        continueBtn.onClick.AddListener(() =>
        {
            onContinue();
            SetVisibilityContinueWindow(false);
        });
    }

    public void SetVisibilityContinueWindow(bool visible)
    {
        continueObj.SetActive(visible);
        background.SetActive(visible);
    }

    public void SetVisibilityPlayWindow(bool visible)
    {
        background.SetActive(visible);
        playObj.SetActive(visible);
    }
}