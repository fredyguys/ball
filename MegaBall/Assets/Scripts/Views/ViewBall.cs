﻿using System;
using DG.Tweening;
using UnityEngine;

public class ViewBall : MonoBehaviour, IViewBall
{
    public Vector3 Positions => transform.localPosition;
    private Action<Vector3> OnReachedPosition;
    private Action OnReachedDestination;

    private float timeToReachPoint = 0.05f;

    public void SetStartPosition(Vector3 position)
    {
        transform.localPosition = new Vector3(position.x, 1.5f, position.z);
    }

    public void MoveTo(Vector3[] path)
    {
        var duration = timeToReachPoint * path.Length;

        transform.DOPath(path, duration).
            OnWaypointChange((i) => OnReachedPosition(path[i])).
            OnComplete(() => OnReachedDestination());
    }

    public void SetPartOfWayReachedAction(Action<Vector3> onReachedPosition)
    {
        OnReachedPosition = onReachedPosition;
    }

    public void SetReachDestinationAction(Action onReachedDestination)
    {
        OnReachedDestination = onReachedDestination;
    }
}