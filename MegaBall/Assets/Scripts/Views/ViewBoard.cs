﻿using DG.Tweening;
using System;
using System.Collections.Generic;
using UnityEngine;

public class ViewBoard : MonoBehaviour, IViewBoard
{
    [SerializeField] private List<Transform> cubes;

    [Header("Material for open area")]
    [SerializeField] private Material greyMat;
    [SerializeField] private Material greenMat;

    public bool IsAllAreaOpened
    {
        get
        {
            foreach (var cube in cubes)
            {
                if (cube.GetComponent<Renderer>().material.color == greyMat.color)
                {
                    return false;
                }
            }

            return true;
        }
    }

    public void SetUpBoard(Action onPrepared)
    {
        foreach (Transform child in cubes)
        {
            child.GetComponent<Renderer>().material = greyMat;
        }
        onPrepared();
    }

    public bool CanMoveToDirection(MovementDirection direction, Vector3 ballPosition)
    {
        switch (direction)
        {
            case MovementDirection.Up:
                return BoardContainPosition(new Vector3(ballPosition.x, 0f, ballPosition.z + 1));
            case MovementDirection.Down:
                return BoardContainPosition(new Vector3(ballPosition.x, 0f, ballPosition.z - 1));
            case MovementDirection.Left:
                return BoardContainPosition(new Vector3(ballPosition.x - 1, 0f, ballPosition.z));
            case MovementDirection.Right:
                return BoardContainPosition(new Vector3(ballPosition.x + 1, 0f, ballPosition.z));
        }
        return false;
    }

    public List<Vector3> GetPath(MovementDirection direction, int step, List<Vector3> path, Vector3 ballPosition)
    {
        switch (direction)
        {
            case MovementDirection.Up:
                var upPos = new Vector3(ballPosition.x, 0f, ballPosition.z + step);
                if (BoardContainPosition(upPos))
                {
                    step++;
                    path.Add(upPos);
                    return GetPath(direction, step, path, ballPosition);
                }
                return path;
            case MovementDirection.Down:
                var dowsPos = new Vector3(ballPosition.x, 0f, ballPosition.z - step);
                if (BoardContainPosition(dowsPos))
                {
                    step++;
                    path.Add(dowsPos);
                    return GetPath(direction, step, path, ballPosition);
                }
                return path;
            case MovementDirection.Left:
                var leftPos = new Vector3(ballPosition.x - step, 0f, ballPosition.z);
                if (BoardContainPosition(leftPos))
                {
                    step++;
                    path.Add(leftPos);
                    return GetPath(direction, step, path, ballPosition);
                }
                return path;
            case MovementDirection.Right:
                var rightPos = new Vector3(ballPosition.x + step, 0f, ballPosition.z);
                if (BoardContainPosition(rightPos))
                {
                    step++;
                    path.Add(rightPos);
                    return GetPath(direction, step, path, ballPosition);
                }
                return path;
        }
        return null;
    }

    public Vector3 GetStartBallPosition()
    {
        return cubes[UnityEngine.Random.Range(0, cubes.Count)].transform.localPosition;
    }

    public void OpenArea(Vector3 position)
    {
        foreach (var cube in cubes)
        {
            if (cube.transform.localPosition == position)
            {
                cube.GetComponent<Renderer>().material = greenMat;
            }
        }
    }

    public void RotateCubes(int z)
    {
        foreach (var cube in cubes)
        {
            var quaternion = new Quaternion();
            quaternion.eulerAngles = new Vector3(0f, 0f, z);
            cube.transform.DOLocalRotateQuaternion(quaternion, 1f);
        }
    }

    private bool BoardContainPosition(Vector3 position)
    {
        foreach (var cube in cubes)
        {
            if (cube.transform.localPosition == position)
            {
                return true;
            }
        }

        return false;
    }
}