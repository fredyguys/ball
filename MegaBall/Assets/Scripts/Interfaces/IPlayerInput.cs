﻿public interface IPlayerInput
{
    MovementDirection Direction { get; }
}