﻿using System;
using UnityEngine;

public interface IViewBall
{
    void SetReachDestinationAction(Action onReachedDestination);
    void SetPartOfWayReachedAction(Action<Vector3> onReachedPosition);
    void MoveTo(Vector3[] path);
    void SetStartPosition(Vector3 position);

    Vector3 Positions { get; }
}
