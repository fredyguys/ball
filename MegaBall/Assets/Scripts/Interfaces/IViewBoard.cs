﻿using System;
using System.Collections.Generic;
using UnityEngine;

public interface IViewBoard
{
    void SetUpBoard(Action onPrepared);
    void OpenArea(Vector3 position);
    void RotateCubes(int z);

    bool IsAllAreaOpened { get; }
    bool CanMoveToDirection(MovementDirection direction, Vector3 ballPosition);

    Vector3 GetStartBallPosition(); 
    List<Vector3> GetPath(MovementDirection direction, int step, List<Vector3> path, Vector3 ballPosition);
}
