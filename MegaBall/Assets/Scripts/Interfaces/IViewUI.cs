﻿using System;

public interface IViewUI
{
    void SetPlayAction(Action onPlay);
    void SetContinueAction(Action onContinue);
    void SetVisibilityContinueWindow(bool visible);
    void SetVisibilityPlayWindow(bool visible);
}